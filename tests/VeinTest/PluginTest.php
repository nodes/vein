<?php

namespace VeinTest;

use Vein\Plugin;

/**
 * Class PluginTest
 * @package VeinTest
 */
class PluginTest extends \PHPUnit_Framework_TestCase
{
    public function testConfig()
    {
        $config = Plugin::getConfig();
        $this->assertTrue(is_array($config));

        $mauticConfig = Plugin::getConfig('mauticApi');
        $this->assertEquals('http://mautic.dev', $mauticConfig['url']);
    }
}
