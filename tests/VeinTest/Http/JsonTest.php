<?php

namespace VeinTest\Http;

use Vein\Http\Json;

class JsonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Json
     */
    private $json;

    /**
     * @var array
     */
    private $person;

    /**
     *
     */
    public function setup()
    {
        $this->json = new Json();

        $this->person = [
            'firstName' => 'john',
            'lastName'  => 'doe'
        ];
    }

    /**
     * Test Json Object Instance
     */
    public function testInstance()
    {
        $this->assertInstanceOf('\Vein\Http\Json', $this->json);
    }

    /**
     * Test Getter and Setter of Data field
     */
    public function testDataCrud()
    {
        sort($this->person);
        $this->json->setData($this->person);

        $actual = $this->json->getData();
        $this->assertEquals($this->person, $actual);

        //Add and age value
        $this->json->add('age', 21);
        $this->assertEquals(21, $this->json->get('age'));

        //Remove data field
        $this->json->remove('age');
        $this->assertEquals($this->person, $actual);

    }

    /**
     * Test Json Serialize
     */
    public function testJsonSerialize()
    {
        $actual   = json_encode($this->json);
        $expected = json_encode($this->json);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test Array Operations
     */
    public function testArrayOperations()
    {
        $this->json->setData($this->person);

        $person = [];
        //Test Loop
        foreach($this->json as $key => $value)
        {
            $person[$key] = $value;
        }
        $this->assertEquals($person, $this->json->getData());

        //Test isset
        $this->assertEquals(true, isset($this->json['firstName']));

        //Test unset
        $this->json->add('age', 21);


        //Get Set Value
        $this->json['age'] = 100;
        $this->assertEquals(100, $this->json['age']);

        //Unset Value
        unset($this->json['age']);
        $this->assertEquals(false, isset($this->json['age']));

        //Exist Check
        $this->assertInstanceOf('\Vein\Http\Json', $this->json);
    }

}