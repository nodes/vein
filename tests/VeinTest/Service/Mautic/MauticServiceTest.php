<?php

namespace VeinTest\Service\Mautic;

use Mautic\Api\Contacts;
use Vein\Service\Mautic\MauticService;

/**
 * Class MauticServiceTest
 * 
 * @package VeinTest\Service\Mautic
 */
class MauticServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MauticService
     */
    protected $api;

    public function setUp()
    {
        $this->api = new MauticService();
    }

    /**
     * Test Crud Operations
     */
    public function testContactsApiCrud()
    {
        /** @var Contacts $contactApi */
        $contactApi = $this->api->newApi('contacts');
        $fields = $contactApi->getFieldList();
        $status = !isset($response['error']);
        $this->assertTrue($status);

        //Create
        $data = [
            'firstname'     => 'John',
            'email'         => 'php-unit@oasandbox.info',
            'ipAddress'     => '127.0.0.1'
        ];

        $response = $contactApi->create($data);
        $newContact = $response[$contactApi->itemName()];
        $this->assertTrue($newContact['isPublished']);

        $response = $contactApi->edit($newContact['id'], ['firstname' => 'Resig']);
        $contact = $response[$contactApi->itemName()];
        $this->assertEquals('Resig', $contact['fields']['core']['firstname']['value']);

        $response = $contactApi->get($newContact['id']);
        $contact = $response[$contactApi->itemName()];
        $this->assertEquals('Resig', $contact['fields']['core']['firstname']['value']);

        $response = $contactApi->delete($newContact['id']);
        $contact = $response[$contactApi->itemName()];
        $this->assertEmpty($contact['id']);
    }

    /**
     * Test form proxy
     */
    public function testFormProxy()
    {
        //arg 1: Mautic Form ID
        //arg 2: POST VARIABLES based on form field names
        $response = $this->api->formProxy(1, [
            'email_address' => 'php-unit@oasandbox.info'
        ]);

        $this->assertNotEmpty($response);
    }
}
