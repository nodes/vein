<?php
require_once("../../../pimcore/cli/startup.php");
Zend_Session::$_unitTestEnabled = true;

if (!defined("VEIN_TEST_PATH")) {
    define('VEIN_TEST_PATH', realpath(dirname(__FILE__)));
}