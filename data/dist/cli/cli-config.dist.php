<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once 'pimcore/cli/startup.php';

$container = \Zend_Controller_Front::getInstance()->getParam('ServiceContainer');
$em = $container->get('vein.entity_manager');

if(!$em) {
    echo 'Vein doctrine is disabled or not configured properly';
    exit;
}

return ConsoleRunner::createHelperSet($em);