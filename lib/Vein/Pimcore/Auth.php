<?php

namespace Vein\Pimcore;

use Vein\Pimcore\Auth\ObjectAdapter;
use Vein\Pimcore\Auth\ObjectInterface;

/**
 * Class Auth
 * @package Vein\Pimcore
 */
class Auth
{
    /**
     * @var ObjectAdapter
     */
    protected static $adapter;

    /**
     * @param $username
     * @param $password
     * @param bool $remember
     * @param null $adapter
     * @return bool
     */
    public static function login($username, $password, $remember = false, $adapter = null)
    {
        if (null === $adapter) {
            $adapter = self::$adapter;
        }
        $adapter->setUsername($username);
        $adapter->setPassword($password);

        $auth = \Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        if ($result->isValid()) {
            if ($remember) {
                \Zend_Session::rememberMe((is_numeric($remember) ? $remember : false));
            } else {
                \Zend_Session::forgetMe();
            }
            return true;
        }
        return false;
    }

    /**
     * Set Default Adapter
     *
     * @param $objectClassName
     * @param $usernameField
     * @param $passwordField
     * @return ObjectAdapter
     */
    public static function setupDefaultAdapter($objectClassName, $usernameField, $passwordField)
    {
        self::$adapter = new ObjectAdapter($objectClassName, $usernameField, $passwordField);
        return self::$adapter;
    }

    /**
     * Logout
     *
     */
    public static function logout()
    {
        \Zend_Auth::getInstance()->clearIdentity();
    }

    /**
     * @return bool
     */
    public static function check()
    {
        return \Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * @return mixed|null
     */
    public static function getUser()
    {
        return \Zend_Auth::getInstance()->getIdentity();
    }

    /**
     * @param  $user
     * @return bool
     * @throws \Exception
     */
    public static function forceLogin($user)
    {
        if (\Zend_Auth::getInstance()->hasIdentity()) {
            \Zend_Auth::getInstance()->clearIdentity();
        }

        \Zend_Auth::getInstance()->getStorage()->write($user);
        return true;
    }

    /**
     * @param $password
     * @return string
     */
    public static function encryptPassword($password)
    {
        return md5($password);
    }
}