<?php

namespace Vein\Pimcore\Auth;

/**
 * Interface ObjectInterface
 * @package Vein\Pimcore\Auth
 */
interface ObjectInterface
{
    /**
     * @return mixed
     */
    public function getUsernameField();

    /**
     * @return mixed
     */
    public function getPasswordField();
}