<?php

namespace Vein\Pimcore\Auth;

use Pimcore\Resource\Mysql;
use Vein\Pimcore\Auth;
use Vein\Pimcore\Auth\ObjectInterface;

/**
 * Class PimcoreObject
 *
 * @package Vein\Auth\Adapter
 */
class ObjectAdapter implements \Zend_Auth_Adapter_Interface
{
    /**
     * @var \Pimcore\Model\Object\AbstractObject
     */
    protected $userObject;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $objectName;

    /**
     * @var string
     */
    protected $usernameField;

    /**
     * @var string
     */
    protected $passwordField;

    /**
     * Class Constructor
     * @param $objectName
     * @param $usernameField
     * @param $passwordField
     */
    public function __construct($objectName, $usernameField, $passwordField)
    {
        $this->setObjectName($objectName);
        $this->setUsernameField($usernameField);
        $this->setPasswordField($passwordField);

        \Zend_Db_Table::setDefaultAdapter(Mysql::get()->getResource());
    }

    /**
     * @return \Zend_Auth_Result
     * @throws \Exception
     */
    public function authenticate()
    {
        if (!$this->usernameField || !$this->passwordField || !$this->objectName) {
            throw new \Exception('Object adapter not properly configured');
        }

        $authResultIdentity = null;
        $authResultMessages = array();

        $identities = $this->fetchUser();

        if (count($identities) == 0) {
            $authResultCode = \Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
        } elseif (count($identities) == 1) {

            $identity = $identities[0];
            if ($this->verifyPassword($identity)) {
                $authResultCode = \Zend_Auth_Result::SUCCESS;
                $authResultIdentity = $identity;
            } else {
                $authResultCode = \Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            }

        } else {
            $authResultCode = \Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS;
        }

        return new \Zend_Auth_Result($authResultCode, $authResultIdentity, $authResultMessages);
    }

    /**
     * Fetch User
     */
    public function fetchUser()
    {
        $identityColumn = $this->getUsernameField();
        if (empty($identityColumn)) {
            throw new \Exception('Identity column name cannot be empty');
        }

        $objectName = $this->getObjectName();
        $objectList = "\\Pimcore\\Model\\Object\\$objectName\\Listing";

        /** @var  $list */
        $list = new $objectList();
        $list->setCondition("{$identityColumn} = ?", $this->username);
        return $list->load();
    }

    /**
     * @param $object
     * @return bool
     */
    public function verifyPassword($object)
    {
        $passwordColumn = $this->getPasswordField();
        $getter = 'get' . ucfirst($passwordColumn);
        return ($object->$getter() == Auth::encryptPassword($this->password));
    }


    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getObjectName()
    {
        return $this->objectName;
    }

    /**
     * @param string $objectName
     * @return $this
     */
    public function setObjectName($objectName)
    {
        $this->objectName = ucfirst($objectName);
        return $this;
    }

    /**
     * @return string
     */
    public function getUsernameField()
    {
        return $this->usernameField;
    }

    /**
     * @param string $usernameField
     * @return $this
     */
    public function setUsernameField($usernameField)
    {
        $this->usernameField = $usernameField;
        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordField()
    {
        return $this->passwordField;
    }

    /**
     * @param string $passwordField
     * @return $this
     */
    public function setPasswordField($passwordField)
    {
        $this->passwordField = $passwordField;
        return $this;
    }


}