<?php

namespace Vein\Application\Resource;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\DriverChain;
use Pimcore\Config;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Yaml\Parser;
use Vein\Doctrine\Extension\TablePrefix;

/**
 * Class DoctrineLoader
 * @package Vein\Doctrine
 */
class DoctrineLoader
{
    protected static $listeners = [];
    /**
     * @var mixed
     */
    protected $cache;
    /**
     * @var AnnotationReader
     */
    protected $annotationReader;
    /**
     * @var CachedReader
     */
    protected $cacheReader;
    /**
     * @var AnnotationDriver
     */
    protected $annotationDriver;
    /**
     * @var DriverChain
     */
    protected $driverChain;
    /**
     * @var EventManager
     */
    protected $eventManager;
    /**
     * Default Configuration
     *
     * @var array
     */
    protected $default = [
        'orm' => [
            'auto_mapping'                  => false,
            'auto_generate_proxy_classes'   => true,
            'proxy_namespace'               => 'Proxy',
            'proxy_dir'                     => null,
            'table_prefix'                  => null,
            'mappings'                      => [],
            'connection' => [
                'dbname'    => null,
                'user'      => null,
                'password'  => null,
                'host'      => 'localhost'
            ]
        ],
        'gedmo' => [
            'SluggableListener'         => false,
            'TreeListener'              => false,
            'LoggableListener'          => false,
            'TimestampableListener'     => false,
            'TranslatableListener'      => false,
            'SortableListener'          => false
        ]
    ];
    /**
     * @var array
     */
    protected $config;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Class Constructor
     */
    public function __construct()
    {
        $this->loadConfiguration();

        $this->init();
    }

    /**
     * Load Configuration
     *
     * @return array|mixed
     */
    protected function loadConfiguration()
    {
        if (null === $this->config) {
            $yaml = new Parser();
            $file = PIMCORE_WEBSITE_VAR.'/plugins/vein/config/docvein.yaml';
            $config = $yaml->parse(file_get_contents($file));

            //Doctrine Vein
            if (empty($config['docvein'])) {
                throw new Exception('Invalid configuration file');
            }
            $this->config = array_merge($this->default, $config['docvein']);

            //Setup Proxy Directory
            if (empty($this->config['orm']['proxy_dir'])) {
                $this->config['orm']['proxy_dir'] = PIMCORE_WEBSITE_VAR.'/plugins/vein/cache';
            }

            //Database name
            if (empty($this->config['orm']['connection']['dbname'])) {
                throw new Exception('Invalid database name');
            }

            //Load configuration from pimcore
            if ($this->config['orm']['connection']['dbname'] == '?') {
                $systemConfig = Config::getSystemConfig()->toArray();
                $params = $systemConfig['database']['params'];

                // the connection configuration
                $dbParams = [
                    'driver' => strtolower($systemConfig['database']['adapter']),
                    'user' => $params['username'],
                    'password' => $params['password'],
                    'dbname' => $params['dbname'],
                    'host' => 'localhost',
                ];

                $this->config['orm']['connection'] = $dbParams;
            }

            //Auto generate proxy classes
            if ($this->config['orm']['auto_generate_proxy_classes'] === null) {
                $this->config['orm']['auto_generate_proxy_classes'] = getenv('APPLICATION_ENV') == 'production';
            }
        }

        return $this->config;
    }

    /**
     * Init
     */
    public function init()
    {
        $this->cache            = $this->setupCache();
        $this->annotationReader = $this->setupOrmAnnotationReader();
        $this->cacheReader      = $this->setupCacheReader($this->annotationReader, $this->cache);
        $this->driverChain      = $this->setupDriverChain();

        $this->eventManager     = $this->setupEventManager($this->config);
        $this->setupGedmo($this->eventManager,  $this->cacheReader, $this->driverChain, $this->config);

        $this->annotationDriver = $this->setupAnnotationDriver($this->cacheReader, $this->driverChain, $this->config);

        $doctrineConfig = new Configuration();

        $doctrineConfig->setProxyNamespace('Proxy');
        $doctrineConfig->setProxyDir($this->config['orm']['proxy_dir']);
        $doctrineConfig->setAutoGenerateProxyClasses($this->config['orm']['auto_generate_proxy_classes']);

        $doctrineConfig->setMetadataDriverImpl($this->driverChain);
        $doctrineConfig->setMetadataCacheImpl($this->cache);
        $doctrineConfig->setQueryCacheImpl($this->cache);
        $doctrineConfig->setResultCacheImpl($this->cache);

        \Pimcore::getEventManager()->trigger("vein.doctrine.init", $doctrineConfig);

        $this->em = $this->setupEntityManager($doctrineConfig, $this->eventManager);

        \Pimcore::getEventManager()->trigger("vein.doctrine.entity.ready", $this->em);
    }

    /**
     * @return ApcCache|ArrayCache
     */
    protected function setupCache()
    {
        if (getenv('APPLICATION_ENV') == 'production') {
            if (extension_loaded('apc')) {
                $cache = new \Doctrine\Common\Cache\ApcCache();
            } elseif (extension_loaded('xcache')) {
                $cache = new \Doctrine\Common\Cache\XcacheCache();
            } elseif (extension_loaded('memcache')) {
                $memcache = new \Memcache();
                $memcache->connect('127.0.0.1');
                $cache = new \Doctrine\Common\Cache\MemcacheCache();
                $cache->setMemcache($memcache);
            } elseif (extension_loaded('redis')) {
                $redis = new \Redis();
                $redis->connect('127.0.0.1');
                $cache = new \Doctrine\Common\Cache\RedisCache();
                $cache->setRedis($redis);
            } else {
                $cache = new ArrayCache();
            }
        } else {
            $cache = new ArrayCache();
        }
        $cache->setNamespace("dc2_" . md5($this->config['orm']['cache_id']) . "_"); // to avoid collisions
        return $cache;
    }

    /**
     * @return AnnotationReader
     */
    protected function setupOrmAnnotationReader()
    {
        AnnotationRegistry::registerFile(
            PIMCORE_DOCUMENT_ROOT . '/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
        );

        return new AnnotationReader();
    }

    /**
     * @param $annotationReader
     * @param $cache
     * @return CachedReader
     */
    protected function setupCacheReader($annotationReader, $cache)
    {
        return new CachedReader(
            $annotationReader, // use reader
            $cache // and a cache driver
        );
    }

    /**
     * @return DriverChain
     */
    protected function setupDriverChain()
    {
        return new DriverChain();
    }

    /**
     * @param $config
     * @return EventManager
     */
    protected function setupEventManager($config)
    {
        $evm = new EventManager();
        if(!empty($config['orm']['table_prefix'])) {
            $tablePrefix = new TablePrefix($config['orm']['table_prefix']);
            $evm->addEventListener(Events::loadClassMetadata, $tablePrefix);
        }
        return $evm;
    }

    /**
     * Setup Gedmo
     *
     * @param EventManager $evm
     * @param $annotationReader
     * @param $driverChain
     * @param array $config
     */
    protected function setupGedmo(EventManager $evm, $annotationReader, $driverChain, array $config)
    {
        //Setup Driver Chain
        \Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain,
            $annotationReader
        );

        if(!empty($config['gedmo']['SluggableListener'])) {
            self::$listeners['sluggableListener'] = new \Gedmo\Sluggable\SluggableListener;
            self::$listeners['sluggableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['sluggableListener']);
        }

        if(!empty($config['gedmo']['TreeListener'])) {
            self::$listeners['treeListener'] = new \Gedmo\Tree\TreeListener;
            self::$listeners['treeListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['treeListener']);
        }

        if(!empty($config['gedmo']['LoggableListener'])) {
            self::$listeners['loggableListener'] = new \Gedmo\Loggable\LoggableListener;
            self::$listeners['loggableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['loggableListener']);
        }

        if(!empty($config['gedmo']['TimestampableListener'])) {
            self::$listeners['timestampableListener'] = new \Gedmo\Timestampable\TimestampableListener;
            self::$listeners['timestampableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['timestampableListener']);
        }

        if(!empty($config['gedmo']['TranslatableListener'])) {
            self::$listeners['translatableListener'] = new \Gedmo\Translatable\TranslatableListener;
            self::$listeners['translatableListener']->setTranslatableLocale('en');
            self::$listeners['translatableListener']->setDefaultLocale('en');
            self::$listeners['translatableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['translatableListener']);
        }

        if(!empty($config['gedmo']['SortableListener'])) {
            self::$listeners['sortableListener'] = new \Gedmo\Sortable\SortableListener;
            self::$listeners['sortableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['sortableListener']);
        }

        if (!empty($config['gedmo']['UploadableListener'])) {
            self::$listeners['uploadableListener'] = new \Gedmo\Uploadable\UploadableListener();
            self::$listeners['uploadableListener']->setDefaultPath(PIMCORE_WEBSITE_VAR.'/plugins/upload');
            self::$listeners['uploadableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['uploadableListener']);
        }

        if (!empty($config['gedmo']['IpTraceableListener'])) {
            $ip = @\Pimcore\Tool::getClientIp();
            $ip = (empty($ip)) ? '0.0.0.0' : $ip;

            self::$listeners['ipTraceableListener'] = new \Gedmo\IpTraceable\IpTraceableListener;
            self::$listeners['ipTraceableListener']->setIpValue($ip);
            self::$listeners['ipTraceableListener']->setAnnotationReader($annotationReader);
            $evm->addEventSubscriber(self::$listeners['ipTraceableListener']);
        }
        $evm->addEventSubscriber(new \Doctrine\DBAL\Event\Listeners\MysqlSessionInit());
    }

    /**
     * @param $cachedReader
     * @param $driverChain
     * @param array $config
     * @return AnnotationDriver
     */
    protected function setupAnnotationDriver($cachedReader, $driverChain, array $config)
    {
        $paths = [];
        $namespaces = [];

        if (is_array($config['orm']['mappings'])) {
            foreach ($config['orm']['mappings'] as $key => $map) {
                if (!$map['mapping']) {
                    continue;
                }
                $paths[] = $map['dir'];
                $namespaces[] = $map['prefix'];
            }
        }

        $annotationDriver = new AnnotationDriver(
            $cachedReader,
            $paths
        );

        foreach ($namespaces as $namespace => $v) {
            $driverChain->addDriver($annotationDriver, $v);
        }

        return $annotationDriver;
    }

    /**
     * Setup Entity Manager
     *
     * @param $doctrineConfig
     * @param $evm
     * @return EntityManager
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    protected function setupEntityManager($doctrineConfig, $evm)
    {
        $connection = $this->getDefaultConnectionParams();
        $em = EntityManager::create($connection, $doctrineConfig, $evm);

        $conn = $em->getConnection();
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        \Doctrine\DBAL\Types\Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping('uuid', 'uuid');

        return $em;
    }

    /**
     * @throws \Exception
     */
    protected function getDefaultConnectionParams()
    {
        $connection = $this->config['orm']['connection'];

        if (empty($connection)) {
            throw new \Exception('Docvein default connection params not set');
        }

        return $connection;
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function getListener($name)
    {
        return self::$listeners[$name];
    }

    /**
     * @return mixed
     */
    public function isDisabled()
    {
        return $this->config['disabled'];
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }
}