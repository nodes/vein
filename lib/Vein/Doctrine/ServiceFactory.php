<?php

namespace Vein\Doctrine;

use Vein\Application\Resource\DoctrineLoader;

/**
 * Class ServiceFactory
 * @package Vein\Doctrine
 */
class ServiceFactory
{
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public static function createEntityManager()
    {
        $doctrineLoader = new DoctrineLoader();
        return $doctrineLoader->getEntityManager();
    }
}