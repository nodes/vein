<?php

namespace Vein\Doctrine;

use Vein\Plugin;

/**
 * Class ServiceTrait
 * @package Vein\Doctrine
 */
trait DoctrineServiceTrait
{
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return Plugin::getEntityManager();
    }

    /***
     * @param $name
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository($name)
    {
        return $this->getEntityManager()->getRepository($name);
    }
}