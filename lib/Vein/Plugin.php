<?php
namespace Vein;

use Pimcore\API\Plugin as PluginLib;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Vein\Controller\Plugin\AppFront;

/**
 * Class Plugin
 * @package Vein
 */
class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    /**
     * @var string
     */
    protected static $_serviceConfig;

    /**
     * @var
     */
    protected static $pluginConfig;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public static function getEntityManager()
    {
        return self::getContainer()->get('vein.entity_manager');
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerBuilder
     */
    public static function getContainer()
    {
        return \Zend_Controller_Front::getInstance()->getParam('ServiceContainer');
    }

    /**
     * Get Config
     *
     * @param null $name
     * @return mixed
     * @throws \Exception
     */
    public static function getConfig($name = null)
    {
        if(self::$pluginConfig === null) {
            $file = self::getInstallPath() . '/config/plugin-config.php';

            if(defined('VEIN_TEST_PATH')) {
                $file = PIMCORE_PLUGINS_PATH . '/Vein/_config/plugin-config.php';
            }

            if(!file_exists($file)) {
                throw new \Exception('Vein plugin config file not found ' . $file);
            }
            self::$pluginConfig = require_once $file;
        }
        return ($name === null) ? self::$pluginConfig : self::$pluginConfig[$name];
    }

    /**
     * @return bool|string
     */
    public static function install()
    {
        $path = self::getInstallPath();

        if(!class_exists('Symfony\Component\DependencyInjection\ContainerBuilder')) {
            return "Vein Plugin cannot be installed without the required dependencies";
        }

        if (!is_dir($path . '/installed')) {

            $distPath = self::pluginPath() . '/data/dist';

            if (!is_dir($path)) {
                mkdir($path);
            }

            mkdir($path . '/installed');

            if (!is_dir($path . '/config')) {
                mkdir($path . '/config');
            }

            if (!is_dir($path . '/cache')) {
                mkdir($path . '/cache');
            }

            $serviceConfig = $path . '/config/services.yaml';
            if (!file_exists($serviceConfig)) {
                file_put_contents($serviceConfig, '');
            }

            $docVeinConfig = $path . '/config/docvein.yaml';
            if (!file_exists($docVeinConfig)) {
                file_put_contents($docVeinConfig, '');
            }
        }

        if (self::isInstalled()) {
            return "Vein Plugin successfully installed.";
        } else {
            return "Vein Plugin could not be installed";
        }
    }

    /**
     * @return string
     */
    public static function getInstallPath()
    {
        return PIMCORE_WEBSITE_VAR . '/plugins/vein';
    }

    /**
     * Get Plugin Path
     *
     * @return string
     */
    public static function pluginPath()
    {
        return PIMCORE_PLUGINS_PATH . '/Vein';
    }

    /**
     * @return bool
     */
    public static function isInstalled()
    {
        return is_dir(static::getInstallPath() . '/installed');
    }

    /**
     * @return string
     */
    public static function uninstall()
    {
        rmdir(self::getInstallPath() . '/installed');

        if (!self::isInstalled()) {
            return "Vein Plugin successfully uninstalled.";
        } else {
            return "Vein Plugin could not be uninstalled";
        }
    }

    /**
     * @throws \Zend_EventManager_Exception_InvalidArgumentException
     */
    public function init()
    {
        parent::init();

        $front = \Zend_Controller_Front::getInstance();
        $service = $front->getParam('ServiceContainer');

        if (!$service) {
            $path = PIMCORE_WEBSITE_VAR . '/plugins/vein/config/';
            if (file_exists($path.'services.yaml')) {
                $fileLocator = new FileLocator($path);

                $container = new ContainerBuilder();
                $loader = new YamlFileLoader($container, $fileLocator);
                $loader->load('services.yaml');

                $front->setParam('ServiceContainer', $container);
            }
        }
        \Pimcore::getEventManager()->attach("system.startup", [$this, "systemStartup"], 9999);
    }

    /**
     * @param $e
     */
    public function systemStartup($e)
    {
        $front = \Zend_Controller_Front::getInstance();
        $frontend = \Pimcore\Tool::isFrontend();
        $front->registerPlugin(new AppFront());
    }
}