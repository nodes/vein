<?php
namespace Vein\Service\Intercom;

use Pimcore\Config;

/**
 * Class Intercom
 *
 * @package Vein\Service\Intercom
 */
class Intercom
{
    /**
     * @var string
     */
    protected $appId;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * Intercom constructor
     */
    public function __construct()
    {
        $config = self::getDefaultApiKey();

        $this->appId = $config['appId'];
        $this->apiKey = $config['apiKey'];
    }

    /**
     * Get default key
     *
     * @return array|bool
     * @throws \Exception
     */
    public static function getDefaultApiKey()
    {
        $c = Config::getWebsiteConfig();
        if (empty($c->intercomApiKey) || empty($c->intercomAppId)) {
            return false;
        }

        return ['apiKey' => $c->intercomApiKey, 'appId' => $c->intercomAppId];
    }

    /**
     * Render Script
     */
    public function renderScript()
    {
        $code = $this->guestUser();
        $code .= $this->includeLibrary();

        return $code;
    }

    /**
     * @internal param $appId
     */
    public function guestUser()
    {
        return <<<EOT
        <script>
            window.intercomSettings = {
                app_id: "{$this->appId}"
            };
        </script>
EOT;
    }

    /**
     * Common Js
     */
    public function includeLibrary()
    {
        return <<<EOT
        <script>(function () {
                var w = window;
                var ic = w.Intercom;
                if (typeof ic === "function") {
                    ic('reattach_activator');
                    ic('update', intercomSettings);
                } else {
                    var d = document;
                    var i = function () {
                        i.c(arguments)
                    };
                    i.q = [];
                    i.c = function (args) {
                        i.q.push(args)
                    };
                    w.Intercom = i;
                    function l() {
                        var s = d.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.src = 'https://widget.intercom.io/widget/{$this->appId}';
                        var x = d.getElementsByTagName('script')[0];
                        x.parentNode.insertBefore(s, x);
                    }

                    if (w.attachEvent) {
                        w.attachEvent('onload', l);
                    } else {
                        w.addEventListener('load', l, false);
                    }
                }
            })()</script>
EOT;
    }
}