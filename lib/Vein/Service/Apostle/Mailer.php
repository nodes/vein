<?php

namespace Vein\Service\Apostle;

use Apostle\Mail;

/**
 * Class Mailer
 * @package Vein\Service\Apostle
 */
class Mailer extends Mail
{
    /**
     * @var array
     */
    private static $captureEmailAddresses = null;

    /**
     * @param $template
     * @param array $data
     */
    public function __construct($template, $data = [])
    {
        try {
            \Apostle::instance();
        } catch (\Apostle\UninitializedException $e) {
            $c = \Pimcore\Config::getWebsiteConfig();
            \Apostle::setup($c->apostle);
        }
        parent::__construct($template, $data);
    }

    /**
     * @param null $failure
     * @return bool
     */
    public function deliver(&$failure = null)
    {
        $captureMode = $this->isCaptureMode();
        if (is_array($captureMode)) {
            $queue = new \Apostle\Queue();
            foreach ($captureMode as $email) {
                $mail = clone $this;
                $this->data['note'] = 'Captured Mail: '.$mail->email;
                $mail->email = $email;
                $queue->add($mail);
            }
            $status = $queue->deliver($failure);
        } else {
            $status = parent::deliver($failure);
        }

        if (!$status) {
            if (is_string($failure)) {
                \Logger::error($failure);
            } else {
                if (is_array($failure)) {
                    foreach ($failure as $f) {
                        \Logger::error($failure);
                    }
                }
            }
        }

        return $status;
    }

    /**
     * @param Mail $mail
     */
    public function debug($mail)
    {
        if ($mail instanceof \Apostle\Mail && \Pimcore::inDebugMode()) {
            $filename = PIMCORE_WEBSITE_VAR.'/apostle/'.time().'.txt';
            $data = $mail->toArray();
            $data = json_encode($data, JSON_PRETTY_PRINT);
            file_put_contents($filename, $data);
        }
    }

    /**
     * @return bool
     */
    public function isCaptureMode()
    {
        if (false === self::$captureEmailAddresses) {
            return false;
        }

        $conf = \Pimcore\Config::getWebsiteConfig();
        $emails = trim($conf->captureOutgoingEmailsTo);

        if (empty($emails)) {
            self::$captureEmailAddresses = false;

            return false;
        }

        if (null === self::$captureEmailAddresses) {
            self::$captureEmailAddresses = [];
            foreach (explode(',', $emails) as $emailAddress) {
                self::$captureEmailAddresses[] = $emailAddress;
            }

            return self::$captureEmailAddresses;
        }

        return true;
    }
}