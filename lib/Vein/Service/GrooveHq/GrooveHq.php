<?php
namespace Vein\Service\GrooveHq;

use GrooveHQ\Client;
use Pimcore\Config;
use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * Class GrooveHq
 *
 * @deprecated use Vein\Service\GrooveHq\Groove instead
 * @package Vein\Service\GrooveHq
 */
class GrooveHq extends Client
{
    /**
     * @param string $apiToken
     * @param GuzzleHttpClient|null $guzzleHttpClient
     */
    public function __construct($apiToken = null, GuzzleHttpClient $guzzleHttpClient = null)
    {
        if (null === $apiToken) {
            $apiToken = $this->getApiKey();
        }
        parent::__construct($apiToken, $guzzleHttpClient);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        $c = Config::getWebsiteConfig();
        return $c->grooveHq;
    }
}