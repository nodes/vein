<?php

namespace Vein\Service\GrooveHq;

use Pimcore\Config;

/**
 * Class Groove
 * @package Vein\Service\GrooveHq
 */
class Groove extends \Zend_Http_Client
{
    /**
     * @var string
     */
    protected $url = 'https://api.groovehq.com/v1';

    /**
     * Groove constructor.
     */
    public function __construct()
    {
        parent::__construct($this->url);
    }

    /**
     * @param array $data
     * @return \Zend_Http_Response
     */
    public function getTickets($data = [])
    {
        $this->setRequestUri('/tickets');
        if (!empty($data)) {
            $this->setParameterPost($data);
        }

        return $this->request();
    }

    /**
     * @param array $data
     * @return \Zend_Http_Response
     */
    public function createTicket(array $data)
    {
        $this->setRequestUri('/tickets');
        $this->setParameterPost($data);

        return $this->request('POST');
    }

    /**
     * @param null $method
     * @return \Zend_Http_Response
     */
    public function request($method = null)
    {
        if ($method == 'POST') {
            $this->setParameterPost('access_token', $this->getApiKey());
        } else {
            $this->setParameterGet('access_token', $this->getApiKey());
        }

        return parent::request($method);
    }

    /**
     * @param \Zend_Http_Response $response
     * @return bool|mixed
     */
    public function getArray($response)
    {
        if ($response instanceof \Zend_Http_Response) {
            if (strpos($response->getHeader('Content-type'), 'json')) {
                return json_decode($response->getBody(), true);
            }
        } else {
            if (is_array($response) || is_object($response)) {
                return (array)$response;
            }
        }

        return false;
    }

    /**
     * @param null $method
     * @return bool|mixed
     */
    public function requestJson($method = null)
    {
        $response = $this->request($method);

        return $this->getArray($response);
    }

    /**
     * @return bool|mixed
     */
    public function toArray()
    {
        return $this->getArray($this->getLastResponse());
    }

    /**
     * @return mixed
     */
    protected function getApiKey()
    {
        $c = Config::getWebsiteConfig();

        return $c->grooveHq;
    }

    /**
     * Set Request Uri
     *
     * @param $uri
     * @return $this
     * @throws \Zend_Http_Client_Exception
     */
    protected function setRequestUri($uri)
    {
        $this->setUri($this->url . $uri);

        return $this;
    }
}