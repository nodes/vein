<?php
namespace Vein\Service\Infusionsoft;

use Pimcore\Config;

/**
 * Class Infusionsoft
 * @package Vein\Service\Infusionsoft
 */
class Infusionsoft extends \Infusionsoft\Infusionsoft
{
    /**
     * Class Constructor
     *
     * @param array $config
     */
    public function __construct($config = array())
    {
        if(empty($config)) {
            $config = $this->getConfiguration();
        }
        parent::__construct($config);
    }

    /**
     * Get Configuration
     *
     * @return array|bool
     */
    public function getConfiguration()
    {
        $c = Config::getWebsiteConfig();
        if($c->infusionsoft) {
            $settings = explode('|||', $c->infusionsoft);
            return [
                'clientId'     => $settings[0],
                'clientSecret' => $settings[1],
                'redirectUri'  => $settings[2],
                'debug'        => \Pimcore::inDebugMode()
            ];
        }
        return false;
    }
}
