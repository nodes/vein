<?php
namespace Vein\Service\Mautic;

use Mautic\MauticApi;

/**
 * Class MauticService
 * @package Vein\Service\Mautic
 */
class MauticService extends \Mautic\Auth\ApiAuth
{
    /**
     * @var \Mautic\Auth\AuthInterface
     */
    protected $auth;

    /**
     * @var MauticApi
     */
    protected $api;

    /**
     * @var string
     */
    protected $url;

    /**
     * MauticService constructor.
     */
    public function __construct()
    {
        if (!\Zend_Session::isStarted()) {
            \Zend_Session::start();
        }

        $this->auth = $this->newAuthSession();
        $this->api = new MauticApi();
        $this->url = $this->getApiUrl();
    }

    /**
     * @param $context
     * @return \Mautic\Api\Api
     */
    public function newApi($context)
    {
        return $this->api->newApi($context, $this->auth, $this->url);
    }

    /**
     * @param $formId
     * @param array $parameters
     * @return bool|string
     */
    public function formProxy($formId, array $parameters)
    {
        $parameters['formId'] = $formId;
        $formUrl =  $this->url . '/form/submit?formId=' . $formId;

        $client = new \Zend_Http_Client();
        $client->setUri($formUrl);
        $client->setParameterPost(['mauticform' => $parameters]);
        $client->setHeaders([
            'X-Forwarded-For' => \Pimcore\Tool::getClientIp()
        ]);
        $response = $client->request(\Zend_Http_Client::POST);

        if ($response->getStatus() == 200) {
            return $response->getBody();
        }
        return false;
    }

    /**
     * @param array $parameters
     * @return \Mautic\Auth\AuthInterface|\Mautic\Auth\ApiAuth
     */
    protected function newAuthSession($parameters = [])
    {
        if (empty($parameters)) {
            $config = \Vein\Plugin::getConfig('mauticApi');
            $parameters['userName'] = $config['credential']['userName'];
            $parameters['password'] = $config['credential']['password'];
        }

        $parameters = array_merge(
            [
                'userName' => $parameters['userName'],
                'password' => $parameters['password'],
            ],
            $parameters
        );

        return parent::newAuth($parameters, 'BasicAuth');
    }

    /**
     * Get Api url
     */
    protected function getApiUrl()
    {
        $config = \Vein\Plugin::getConfig('mauticApi');
        return $config['url'];
    }
}