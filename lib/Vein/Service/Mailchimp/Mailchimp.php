<?php

namespace Vein\Service\Mailchimp;

use Pimcore\Config;

/**
 * Class Mailchimp
 *
 * @package Vein\Service\Mailchimp
 */
class Mailchimp extends \Zend_Http_Client
{
    /**
     * @var string
     */
    protected $url = 'https://us1.api.mailchimp.com/3.0';

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $dc = 'us1';

    /**
     * Class Constructor
     *
     * @param null $apiKey
     * @throws \Exception
     */
    public function __construct($apiKey = null)
    {
        if (null === $apiKey) {
            $apiKey = $this->getDefaultApiKey();
            if (empty($apiKey)) {
                throw new \Exception('API key is required');
            }
        }
        $this->setApiKey($apiKey);
        parent::__construct($this->url);
    }

    /**
     * Request
     *
     * @param null $method
     * @return \Zend_Http_Response
     * @throws \Zend_Http_Client_Exception
     */
    public function request($method = null)
    {
        //'Content-Type: application/json'
        $this->setHeaders('Content-Type', 'application/json');
        $this->setAuth('apikey', $this->apiKey);

        return parent::request($method);
    }

    /**
     * @return mixed
     */
    public function getAccountInfo()
    {
        $response = $this->get('/');

        return $response;
    }

    /**
     * @param $method
     * @param $args
     * @return \Zend_Http_Response
     * @throws \Exception
     * @throws \Zend_Http_Client_Exception
     */
    public function __call($method, $args)
    {
        $type = strtoupper($method);
        $supportedTypes = ['GET', 'HEAD', 'PUT', 'POST', 'PATCH', 'DELETE'];

        if (in_array($type, $supportedTypes)) {
            $data = isset($args[1]) ? $args[1] : [];
            $this->setRequestUri($args[0]);

            if (!empty($args[1])) {
                if ($type == 'GET') {
                    $this->setParameterGet($data);
                } else {
                    $this->setRawData(json_encode($data), 'application/json');
                }
            }

            return $this->request($type);
        } else {
            throw new \Exception('Invalid request type');
        }
    }

    /**
     * @param \Zend_Http_Response $response
     * @return bool|mixed
     */
    public function getArray($response)
    {
        if ($response instanceof \Zend_Http_Response) {
            if (strpos($response->getHeader('Content-type'), 'json')) {
                return json_decode($response->getBody(), true);
            }
        } else {
            if (is_array($response) || is_object($response)) {
                return (array)$response;
            }
        }

        return false;
    }

    /**
     * @param null $method
     * @return bool|mixed
     */
    public function requestJson($method = null)
    {
        $response = $this->request($method);

        return $this->getArray($response);
    }

    /**
     * @return bool|mixed
     */
    public function toArray()
    {
        return $this->getArray($this->getLastResponse());
    }

    /**
     * @param $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        list(, $dc) = explode('-', $apiKey);
        $this->url = str_replace($this->dc, $dc, $this->url);
        $this->dc = $dc;
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Set Request Uri
     *
     * @param $uri
     * @return $this
     * @throws \Zend_Http_Client_Exception
     */
    protected function setRequestUri($uri)
    {
        $this->setUri($this->url.$uri);

        return $this;
    }

    /**
     * @return mixed
     */
    protected function getDefaultApiKey()
    {
        $c = Config::getWebsiteConfig();

        return $c->mailchimpApiKey;
    }
}