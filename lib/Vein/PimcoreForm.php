<?php

namespace Vein;

use Formbuilder_Formbuilder;
use Pimcore\Tool;

/**
 * Class PimcoreForm
 *
 * @package Vein
 */
class PimcoreForm extends \Zend_Form
{

    /**
     * @param mixed|null $name
     */
    public function __construct($name)
    {
        $config = $this->load($name, null, false);
        parent::__construct($config);
    }

    /**
     * @param $name
     * @param null $locale
     * @param bool $dynamic
     * @return bool|\Zend_Config_Ini
     */
    private function load($name, $locale = null, $dynamic = false)
    {
        $table = new Formbuilder_Formbuilder();
        $id = $table->getIdByName($name);

        if (is_numeric($id) == true) {
            if ($dynamic == false) {
                if (file_exists(PIMCORE_PLUGINS_PATH . "/Zendformbuilder/data/form/form_" . $id . ".ini")) {
                    $config = new \Zend_Config_Ini(PIMCORE_PLUGINS_PATH . "/Zendformbuilder/data/form/form_" . $id . ".ini",
                        'config');
                    return $config->form;
                }
            } else {

            }
        } else {
            return false;
        }
        return false;
    }
}