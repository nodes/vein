<?php
namespace Vein\Http;

/**
 * Class Utility
 *
 * @package Vein\Http
 */
class Utility
{
    /**
     * @var \Zend_Controller_Request_Abstract
     */
    private $request;

    /**
     * @param \Zend_Controller_Request_Abstract $request
     */
    public function __construct(\Zend_Controller_Request_Abstract $request)
    {
        $this->request = $request;
    }

    /**
     * @return \Zend_Controller_Request_Abstract
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return mixed
     */
    public function getJsonData()
    {
        return $this->extractJson();
    }

    /**
     * @return mixed
     */
    private function extractJson()
    {
        $json = false;

        if ($this->hasJson()) {
            try {
                $json = \Zend_Json::decode($this->getRequest()->getRawBody());
            } catch (\Zend_Json_Exception $e) {
                $json = [$e->getMessage()];
            }
        }
        return $json;
    }

    /**
     * @return string
     */
    public function hasJson()
    {
        if (!$this->getRequest()->getRawBody()) {
            return false;
        }
        return true;
    }
}