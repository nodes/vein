<?php
namespace Vein\Http;

use ArrayAccess;
use Countable;
use IteratorAggregate;
use JsonSerializable;
use Pimcore\Model\Webservice\JsonEncoder;

/**
 * Class Json
 *
 * @package Vein\Http
 */
class Json implements JsonSerializable, IteratorAggregate, ArrayAccess, Countable
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * @var array
     */
    public $response = [];

    /**
     * @param null $data
     */
    public function __construct($data = null)
    {
        $this->loadDefaults();
        $this->data = (null === $data) ? [] : (array)$data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return Json
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param bool $status
     * @return Json
     */
    public function setStatus($status = true)
    {
        $this->response['status'] = $status;

        return $this;
    }

    /**
     * @param $message
     * @return Json
     */
    public function setMessage($message)
    {
        $this->response['message'] = $message;

        return $this;
    }

    /**
     * @param $key
     * @param $value
     *
     * @return Json
     */
    public function add($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     */
    public function get($key)
    {
        return $this->data[$key];
    }

    /**
     * @param $key
     *
     * @return Json
     */
    public function remove($key)
    {
        unset($this->data[$key]);

        return $this;
    }

    /**
     * Load Defaults
     */
    public function loadDefaults()
    {
        $this->response = [
            'code' => 200, //application error
            'data' => $this->data,
            'status' => false, //by default send error response,
            'message' => null,
            'type' => 'success', //info, error, success, warning, critical,
            'debugMode' => \Pimcore::inDebugMode() ? true : false,
        ];
        return $this->response;
    }

    /**
     * @param null $code
     *
     * @throws \Zend_Controller_Response_Exception
     */
    public function flush($code = null)
    {
        $this->response['debugMode'] = \Pimcore::inDebugMode() ? true : false;
        $this->response['data'] = $this->data;
        $response = \Zend_Controller_Front::getInstance()->getResponse();
        $response->setHttpResponseCode((null === $code) ? $this->response['code'] : $code);

        $encoder = new JsonEncoder();
        $encoder->encode($this->response);
    }

    /**
     * @param $msg
     * @param int $code
     * @param string $type
     * @return Json
     */
    public function error($msg, $code = 500, $type = 'error', $data = null)
    {
        $this->data = $data;

        if (is_numeric($msg)) {
            $code = $msg;
            $msg = Status::statusText($code);
        }

        $this->response = [
            'code' => $code,
            'status' => false,
            'message' => $msg,
            'type' => $type
        ];

        return $this;
    }

    /**
     * Form validation failed
     *
     * @param $msg
     * @param int $code
     * @param string $type
     * @param null $data
     * @return Json
     */
    public function validationFailed($msg, $code = 422, $type = 'error', $data = null)
    {
        return $this->error($msg, $code, $type, $data);
    }

    /**
     * Set Error Status
     *
     * @param $code
     * @return Json
     */
    public function statusCode($code)
    {
        $status = false;
        $type = 'error';

        if ($code > 199 && $code < 300) {
            $status = 200;
            $type = 'success';
        }

        $this->response = [
            'code' => $code,
            'status' => $status,
            'message' => Status::statusText($code),
            'type' => $type
        ];

        return $this;
    }

    /**
     * @param int $code
     * @return Json
     */
    public function success($code = 200)
    {
        $this->response = [
            'code' => $code,
            'status' => true,
            'message' => Status::statusText($code),
            'type' => 'success'
        ];

        return $this;
    }

    /**
     * @param \Exception $e
     * @return $this
     * @throws \Exception
     */
    public function debug($e)
    {
        if (\Pimcore::inDebugMode()) {
            \Logger::debug($e);
            if ($e instanceof \Exception) {
                $e = $e->getMessage();
            }
            $this->response['debug']['message'] = $e;
        }

        return $this;
    }

    /**
     * @param $offset
     *
     * @return bool
     */
    public function isExist($offset)
    {
        return $this->offsetExists($offset);
    }

    /**
     * @return mixed|void
     */
    public function jsonSerialize()
    {
        return $this->response;
    }

    /**
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->data);
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (null !== $offset) {
            $this->data[$offset] = $value;
        } else {
            $this->data[] = $value;
        }
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->data);
    }

}