<?php
namespace Vein\Http;

/**
 * Class HttpHelper
 *
 * @package Vein\Http
 */
class HttpHelper
{
    /**
     * @var \Zend_Http_Client
     */
    protected $_adapter;

    /**
     * @return \Zend_Http_Client
     */
    public function requestAdapter()
    {
        if (null === $this->_adapter) {
            $config = [
                'adapter' => 'Zend_Http_Client_Adapter_Curl',
                'curloptions' => [
                    CURLOPT_FOLLOWLOCATION => true
                ],
            ];
            $this->_adapter = new \Zend_Http_Client(null, $config);
        }
        return $this->_adapter;
    }

    /**
     * @param $uri
     * @param array $data
     * @return \Zend_Http_Response
     * @throws \Zend_Http_Client_Exception
     */
    public function jsonRequest($uri, array $data = [])
    {
        $client = $this->requestAdapter();
        $client->setUri($this->assembleUrl($uri));
        $client->setRawData(json_encode($data), 'application/json');
        $response = $client->request('POST');

        $json = null;
        try {
            $json = \Zend_Json::decode($response->getBody());
        } catch (\Zend_Json_Exception $e) {
            $json = "Response did not contain a valid JSON.";
        }
        return ['response' => $response, 'data' => $json, 'code' => $response->getStatus()];
    }

    /**
     * @param $uri
     * @param array $data
     * @return \Zend_Http_Response
     * @throws \Zend_Http_Client_Exception
     */
    public function postRequest($uri, array $data = [])
    {
        $config = [
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => [
                CURLOPT_FOLLOWLOCATION => true
            ],
        ];

        $client = new \Zend_Http_Client($this->assembleUrl($uri), $config);
        $client->setParameterPost($data);
        $response = $client->request('POST');
        return ['response' => $response, 'data' => $response->getBody(), 'code' => $response->getStatus()];
    }

    /**
     * Assemble Uri
     *
     * @param $uri
     * @return string
     */
    private function assembleUrl($uri)
    {
        $host = parse_url($uri, PHP_URL_HOST);
        if (empty($host)) {
            $scheme = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
                $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

            $uri = $scheme . $_SERVER['HTTP_HOST'] . $uri;
        }
        return $uri;
    }
}