<?php

namespace Vein\Asset;

/**
 * Class Image
 * @package Vein\Asset
 */
class Image
{
    /**
     * Container
     */
    public static function generateThumb(array $params = [])
    {
        $id = (int)$params['id'];
        $width = (int)$params['width'];
        $height = (int)$params['height'];
        $name = (string)$params['name'];
        $filter = (string)$params['filter'];
        $default = (string)$params['d'];
        $nocache = (int)$params['nocache'];

        $asset = false;
        if ($id > 0) {
            $asset = \Pimcore\Model\Asset\Image::getById($id);
        }

        if (!$asset || (!$asset instanceof \Pimcore\Model\Asset\Image)) {
            $path = '/vein/default-not-found.png';
            if ($default) {
                switch ($default) {
                    case 'avatar':
                        $path = '/vein/default-avatar.png';
                        break;
                    case 'human':
                        $path = '/vein/default-human.png';
                        break;
                    case 'transparent':
                        $path = '/vein/default-transparent.png';
                        break;
                }
            }
            $asset = \Pimcore\Model\Asset\Image::getByPath($path);
        }

        if (!$asset || !($asset instanceof \Pimcore\Model\Asset\Image)) {
            exit;
        }

        if ($asset->isLocked() && $asset->getPath()[0] == '_') {
            exit;
        }

        $options = [];
        if ($width > 0) {
            $options['width'] = $width;
        }

        if ($height > 0) {
            $options['height'] = $height;
        }

        if (in_array($filter, ['contain', 'cover'])) {
            $options[$filter] = true;
        }

        if (!empty($name)) {
            $options = $name;
        }
        $thumbnail = $asset->getThumbnail($options, false);

        $thumbnailFile = PIMCORE_DOCUMENT_ROOT . $thumbnail;
        $fileExtension = \Pimcore\File::getFileExtension($thumbnailFile);
        if (in_array($fileExtension, array("gif", "jpeg", "jpeg", "png", "pjpeg"))) {
            header("Content-Type: image/" . $fileExtension, true);
        } else {
            header("Content-Type: " . $asset->getMimetype(), true);
        }

        header("Content-Length: " . filesize($thumbnailFile), true);

        if ($nocache < 1) {
            self::thumbnailCacheHeaders();
        }
        while (@ob_end_flush()) {
            ;
        }
        flush();

        readfile($thumbnailFile);
        exit;
    }

    /**
     * @param int $lifetime
     */
    public static function thumbnailCacheHeaders($lifetime = 300)
    {
        \Zend_Controller_Front::getInstance()->getResponse()->clearAllHeaders();;

        header("Cache-Control: public, max-age=" . $lifetime, true);
        header("Expires: " . \Zend_Date::now()->add($lifetime)->get(\Zend_Date::RFC_1123), true);
        header("Pragma: ");
    }
}