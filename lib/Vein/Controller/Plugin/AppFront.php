<?php
namespace Vein\Controller\Plugin;

use Pimcore\Tool;
use Vein\Service\Intercom\Intercom;

/**
 * Class AppFront
 * @package Vein\Controller\Plugin
 */
class AppFront extends \Zend_Controller_Plugin_Abstract
{
    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @return bool
     */
    public function disable()
    {
        $this->enabled = false;

        return true;
    }

    /**
     * Replace the contents
     */
    public function dispatchLoopShutdown()
    {
        if (!Tool::isFrontend()) {
            return;
        }

        if (!Tool::isHtmlResponse($this->getResponse())) {
            return;
        }

        if(\Zend_Registry::isRegistered("pimcore_editmode")) {
            $editmode = \Zend_Registry::get("pimcore_editmode");
            if ($editmode) {
                return;
            }
        }

        $body = $this->getResponse()->getBody();

        if (Intercom::getDefaultApiKey()) {
            $modified = false;

            if (strpos($body, 'vein-intercom')) {
                $intercom = new Intercom();
                $code = $intercom->renderScript();

                $headEndPosition = stripos($body, "</body>");
                if ($headEndPosition !== false) {
                    $body = substr_replace($body, $code."</body>", $headEndPosition, 7);
                }
                $modified = true;
            }

            if ($modified) {
                $this->getResponse()->setBody($body);
            }
        }

        return;
    }

}
